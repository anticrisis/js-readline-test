import readline from "readline";

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: "> "
});

rl.prompt();

rl.on("line", line => {
    console.log(`Echo: ${line}`);
    rl.prompt();
});

rl.on("close", () => {
    console.log("closing");
    process.exit(0);
});
